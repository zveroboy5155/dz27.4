#include "DZ_27.4.h"
#include <vector>
#include <string>


#define DBG(data) std::cout <<"DBG!!!" << __func__ << ": " << __LINE__ << " " << data << std::endl;

class Elf {
	std::string name;
	//Branch *branch;
	//int homeNum;
	//Home *home;
public:
	Elf() = default;
	Elf(std::string name) {
		this->name = name;
	}
	static std::string getName(Elf *elf) {
		return elf->name;
	}
};

class Branch {
	Branch *parent;
	std::vector <Branch*> child;
	std::vector<Elf> residents;
	//elf *residents[5]

public:

	Branch() {
		int childNum;

		this->parent = nullptr;
		//�� ������ ���� 3-5
		//childNum = rand() % 2 + 3;
		childNum = 2;
		DBG(" ���-�� ������� ������: " << childNum);
		child.resize(childNum);
		for (auto &it : child) {
			Branch *chBranch = new Branch(this);
			it = chBranch;
		}
		/*Branch *chBranch = new Branch(this);
		this->child = chBranch;*/

	}
	Branch(Branch* parent) {
		//if (!parent) return;
		int resNumber, childNum;

		std::cout << "������� ���-�� ����� �� �����" << std::endl;
		std::cin >> resNumber;
		this->residents.resize(resNumber);
		this->parent = parent;
		//this->child = nullptr;
		//parent->child = this;
		if (getTopBranch() != parent) return;
		//�� ������ ���� 2-3
		childNum = rand() % 2 + 2;
		child.resize(childNum);
		DBG(" ���-�� ������� ������: " << childNum);
		for (auto &it : child) {
			Branch *chBranch = new Branch(this);
			it = chBranch;
		}
		/*Branch *chBranch = new Branch(this);
		this->child = chBranch;*/
	}

	void addResidents() {
		std::string name;
		for(auto &it: residents){
			std::cout << "��� ������ ������ ��� None ��� �������� ����" << std::endl;
			std::cin >> name;
			if (name == "None") continue;
			Elf newResident(name);
			it = newResident;
		}
		if (this->child.empty()) return;
		
		for(auto &it : child) it->addResidents();
	}

	Branch* getTopBranch() {
		if (this->parent == nullptr) return nullptr;
		if (this->parent->parent == nullptr) 
			return this->parent;
		
		return parent->getTopBranch();
	}

	int searchNeighbor(std::string &name) {
		int neighbor = 0;
		bool searchFl = false;
		std::string elfName;

		if (this == getTopBranch()) return searchNeighbor(name);
		for (auto it : residents) {
			elfName = Elf::getName(&it);
			if(elfName != "None") neighbor++;
			if(elfName == name) searchFl = true;
		}
		if (searchFl) return neighbor - 1;
		if(this->child.empty()) return -1;

		for (auto it : child) {
			neighbor = it->searchNeighbor(name);
			if (neighbor > -1) return neighbor;
		}
		return -1;
	}
};

void task1(){
	Branch tree[5];
	std::string elfName;
	int neighbor, i = 1;
	
	for (auto &it : tree) {
		std::cout << "�������� ������ �" << i << std::endl;
		it.addResidents();
		i++;
	}
	while (1) {
		std::cout << "������� ��� ����� ��� �������� ���� ������� " << std::endl;
		std::cin >> elfName;

		for (auto it : tree) {
			neighbor = it.searchNeighbor(elfName);
			if (neighbor != -1) break;
		}
		std::cout << ((neighbor == -1) ? "������ ����� ���" : ("���������� ������� " + std::to_string(neighbor))) << std::endl;
		std::cout << "������ ��������� ����� (Yes/No)" << std::endl;
		std::cin >> elfName;
		if (elfName == "No" || elfName == "NO") break;
	}
}