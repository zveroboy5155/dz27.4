#include "DZ_27.4.h"
#include <vector>
#include <string>

enum Color {
	None = 0,
	Red,
	Blue,
	Green,
	Pink
};

std::istream& operator>>(std::istream& is, Color& f)
{
	std::string s;
	is >> s;
	if (s == "None") f = Color::None;
	else if (s == "Red") f = Color::Red;
	else if (s == "Blue") f = Color::Blue;
	else if (s == "Green") f = Color::Green;
	else if (s == "Pink") f = Color::Pink;
	return is;
}


class Figure;
class Circle;
class Square;
class Triangle;
class Rectangl;

class Figure {
protected:
	int x_pos, y_pos;
	Color color = None;
public:
	Figure() {
		std::cout << "������� ���������� ������ ������" << std::endl;
		std::cin >> x_pos >> y_pos;
		std::cout << "������� ���� ������(0 - ��� �����, 1 - �������, 2 - �����, 3 - ������, 4 - �������)" << std::endl;
		std::cin >> color;
		//this->color = col;
	}
	virtual double getArea() { return 0; };
	virtual void makeOutFig() {};
};
//����
class Circle:Figure {
	double rad;
public:
	Circle() {
		std::cout << "������� ������ �����" << std::endl;
		std::cin >> rad;
	}
	double getArea() {
		return atan(1) * 4 * rad;
	}
	void makeOutFig() {
		std::cout << "������ ��������������, ������������ ������ "<< this->x_pos << " " << this->y_pos << std::endl;
		std::cout << "��� �������������� - �������, ������ �������: " << rad + 1 << std::endl;
	};
};
//�������
class Square:Figure {
	double lenght;
public:
	Square() {
		std::cout << "������� ������ ������� ��������" << std::endl;
		std::cin >> lenght;
	}
	double getArea() {
		return (this->lenght* this->lenght);
	}
	void makeOutFig() {
		std::cout << "������ ��������������, ������������ ������ " << this->x_pos << " " << this->y_pos << std::endl;
		std::cout << "��� �������������� - �������, ������ �������: " << lenght + 1 << std::endl;
	};
};
//�����������
class Triangle:Figure {
	double lenght;
public:
	Triangle() {
		std::cout << "������� ������ ������� ������������" << std::endl;
		std::cin >> lenght;
	}
	double getArea() {
		return this->lenght * this->lenght * std::sqrt(3) / 4;
	}
	void makeOutFig() {
		std::cout << "������ ��������������, ������������ ������ " << this->x_pos << " " << this->y_pos << std::endl;
		std::cout << "��� �������������� - �������, ������ �������: " << (this->lenght*0.5) + 1 << std::endl;
	};
};
//�������������
class Rectangle:Figure {
	double len, with;
public:
	Rectangle() {
		std::cout << "������� ����� ������ ��������������" << std::endl;
		std::cin >> this->len >> this->with;
	}
	double getArea() {
		return len * with;
	}
	void makeOutFig() {
		std::cout << "������ ��������������, ������������ ������ " << this->x_pos << " " << this->y_pos << std::endl;
		std::cout << "����� ������: " << len << " " << with << std::endl;
	};
};

void task2() {
	Figure *figure;
	std::string cmd;

	while (1) {
		std::cout << "������� ������ � ������� ����� �������� (circle, square, triangle, rectangle)" << std::endl;
		std::cin >> cmd;
		if (cmd == "circle") figure = (Figure*)new Circle;
		else if(cmd == "square") figure = (Figure*)new Square;
		else if (cmd == "triangle") figure = (Figure*)new Triangle;
		else if (cmd == "rectangle") figure = (Figure*)new Rectangle;
		else { 
			std::cout << "����������� ������ " << cmd << std::endl;
			continue;
		}
		std::cout << "������� ����� " << figure->getArea() << std::endl;
		figure->makeOutFig();
		delete(figure);
		std::cout << "������ ��������� (Yes/No)?" << std::endl;
		std::cin >> cmd;
		if (cmd == "No" || cmd == "NO") break;

	}
}