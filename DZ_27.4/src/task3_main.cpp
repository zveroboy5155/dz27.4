#include "DZ_27.4.h"
#include <vector>
#include <string>

class Task {
	std::string dsc;
	public:
		void PutDsc(std::string newDesc) { this->dsc = newDesc; }
};

class Worker {
protected:
	std::string name;
public:
	Worker() {
		std::string newName;
		
		std::cout << "������� ��� " << std::endl;
		std::cin >> newName;

		this->name = newName;
	}
	virtual void TakeCmd(int id) {};
};

class OrdWorker : Worker {
	Task task;
	bool busyFl = false;
	char task_descs[3] = { 'A', 'B', 'C' };
public:
	OrdWorker(std::string m_name) { std::cout << this->name << " ������ �� ������ � ������� � " << m_name << std::endl; }
	bool isBusy() { return busyFl; }
	void TakeCmd(int id) {
		if (id < 0 || id > 2) return;
		this->task.PutDsc(std::to_string(this->task_descs[id]));
		std::cout << "��������� " << this->name << " ��������� ������ "
			<< this->task_descs[id] << std::endl;
		this->busyFl = true;
	}
};

class Manager:Worker {
	std::vector <OrdWorker> workers;
	int UId;
public:
	Manager(int Uid) {
		int workNum;
		this->UId = Uid;

		std::cout << "���������� �������� " << this->name << std::endl;
		std::cout << "������� ���-�� ������� � " << Uid + 1 << " �������" << std::endl;
		std::cin >> workNum;
		std::cout << "�������� ������� ���������� ���������" << std::endl;
		for (int i = 0; i < workNum; i++) {
			OrdWorker worker(this->name);
			workers.push_back(worker);
		}
	}
	void TakeCmd(int id) {
		bool freWorkFl = true;
		srand(id + this->UId);
		int tasksCount = rand() % (this->workers.size() + 1) + 1;
		
		std::cout << "�������� " << this->name << " ������ ��������� " << id 
			<< " � ������������ �� ������ ��� �������" << std::endl;
		for (int i = 1; i < tasksCount, freWorkFl; i++){
			if (workers[workers.size() - 1].isBusy()) {
				freWorkFl = false;
				continue;
			}
			for(auto &worker:workers){
				if (worker.isBusy()) continue;
				worker.TakeCmd(rand() % 3); //0-A, 1-B, 2-C
				break;
			}
		}
	}
};

class Director:Worker {
	std::vector <Manager> managers;
public:
	Director() {
		int teamNum;
		
		std::cout << "���������� �������� "<< this->name << std::endl;
		std::cout << "������� ���-�� ������ ����������" << std::endl;
		std::cin >> teamNum;
		std::cout << "�������� ������� ����������" << std::endl;
		for (int i = 0; i < teamNum; i++) {
			Manager manager(i);
			this->managers.push_back(manager);
		}
	}
	void TakeCmd(int id) {
		for (auto &it : managers) it.TakeCmd(id);
	}
};

void task3() {
	int cmd;

	std::cout << "������ �����, ��������� ���������" << std::endl;
	Director director;
	
	setlocale(0, " ");
	while (1) {
		std::cout << "������� ������� �� ���������" << std::endl;
		std::cin >> cmd;
		director.TakeCmd(cmd);
		std::cout << "������ ���������� 1/0?" << std::endl;
		std::cin >> cmd;
		if (cmd == 0) break;

	}
}